package com.example.androiddownloadmanager

import android.content.ClipboardManager
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androiddownloadmanager.databinding.FragmentHomeBinding
import java.io.*
import java.net.URL

class Home : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        binding.edtMy.setStartIconOnClickListener {
            val a = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            binding.tdtMy.setText(a.text)
            viewModel.getStatus(a.text.toString())
        }
        binding.btnDownload.setOnClickListener {
            viewModel.startDownload(binding.tdtMy.text.toString())
        }

        viewModel.percentOfDownload.observe(viewLifecycleOwner, Observer {
//            binding.circleProgress.progress = it
            binding.circleProgress.setProgressWithAnimation(it)
        })
        return binding.root
    }
}
class MyDownload(val txt: TextView) : AsyncTask<String, String, String>() {
    override fun doInBackground(vararg p0: String?): String {

        // params[0] : url_to_download
        return try {
            val url = URL(p0.get(0))
            val connection = url.openConnection()
            connection.connect()
            val lengthOfFile = connection.contentLength

            //
            val input =
                BufferedInputStream(url.openStream(), 8 * 1024)
            val filename: String =
                p0.get(0)!!.substring(p0.get(0)!!.lastIndexOf('/') + 1)
            var outFile = File(
                Environment.getExternalStorageDirectory()
                    .absolutePath,
                filename
            )
            if (!outFile.parentFile.exists()) {
                outFile.parentFile.mkdirs()
            }
            if (outFile.exists()) {
                // myfile.txt -> myfile_2.txt
                val ext =
                    if (filename.contains(".")) filename.substring(filename.lastIndexOf('.')) else ""
                val name = if (filename.contains(".")) filename.substring(
                    0,
                    filename.lastIndexOf('.')
                ) else filename
                outFile = File(
                    Environment.getExternalStorageDirectory()
                        .absolutePath,
                    "$name-2$ext"
                )
            }
            val output: OutputStream = FileOutputStream(outFile)
            val buf = ByteArray(1024)
            var count = 0
            var downloaded = 0
            while (input.read(buf).also { count = it } != -1) {
                downloaded += count
                publishProgress((downloaded * 100 / lengthOfFile).toString()) // percent
                output.write(buf, 0, count)
            }
            output.flush()
            outFile.absolutePath
        } catch (e: IOException) {
            e.message.toString()
        }
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        txt.append("$result \n")
    }

    override fun onProgressUpdate(vararg values: String?) {
        super.onProgressUpdate(*values)
        txt.append("${values[0]} \n")

    }
}
