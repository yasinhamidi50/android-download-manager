package com.example.androiddownloadmanager

import android.content.Context
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androiddownloadmanager.network.ADM
import com.example.androiddownloadmanager.network.OnDownloading
import kotlinx.coroutines.*
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.*

class HomeViewModel : ViewModel() {
    private val BASE_DIRECTORY: String = "AndroidDownloadManager"
    private val PATH: String = Environment.getExternalStorageDirectory().absolutePath
    private val TAVAN: Double = 2.0
    private var input: BufferedInputStream? = null
    private val timer = Timer()

    val job = Job()
    val downloadScope = CoroutineScope(job + Dispatchers.Main)

    private val _sizeOfContent = MutableLiveData<Double>()
    val sizeOfContent: LiveData<Double>
        get() = _sizeOfContent

    private val _nameOfContent = MutableLiveData<String>()
    val nameOfContent: LiveData<String>
        get() = _nameOfContent

    private val _percentOfDownload = MutableLiveData<Int>()
    val percentOfDownload: LiveData<Int>
        get() = _percentOfDownload

    private val _timeOfDownload = MutableLiveData<Int>()
    val timaOfDownload: LiveData<Int>
        get() = _timeOfDownload
    private val _speedOfDownload = MutableLiveData<Float>()
    val speedOfDownload: LiveData<Float>
        get() = _speedOfDownload

    fun getStatus(downloadLink: String) {
        downloadScope.launch {
            _nameOfContent.postValue(
                withContext(Dispatchers.IO) {
                    try {
                        val url = URL(downloadLink)
                        val conection = url.openConnection()
                        conection.connectTimeout = 10000
                        conection.connect()
                        _sizeOfContent.postValue(
                            conection.contentLength / Math.pow(
                                1024.0,
                                TAVAN
                            )
                        )
                        val splitedDownloadLink = downloadLink.split('/')
                        splitedDownloadLink[splitedDownloadLink.lastIndex]
                    } catch (e: Exception) {
                        "${e.message}"
                    }
                })
        }
    }


    fun startDownload(downloadLink: String) {
        val adm = ADM(downloadLink)
        downloadScope.launch {
            adm.start(object : OnDownloading {
                override fun onStart(fileName: String) {
                    _nameOfContent.postValue(fileName)
                }

                override fun onError(error: Error) {
                    _nameOfContent.postValue(error.message)
                }

                override fun onSuccessful(response: String) {
                    _nameOfContent.postValue(response)
                }

                override fun onUpdate(percentOfDownload: Int, speed: Float) {
                    _percentOfDownload.postValue(percentOfDownload)
                    _speedOfDownload.postValue(speed)
                }
            })

        }

    }


}