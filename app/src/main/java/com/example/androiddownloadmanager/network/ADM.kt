package com.example.androiddownloadmanager

import android.os.Environment
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.net.URLConnection
import java.util.*

class ADM(private val url: URL) {
    constructor(url: String) : this(URL(url))

    private val TIME_OUT = 10000
    private val PATH = Environment.getExternalStorageDirectory().absolutePath
    private val BASE_DIRECTORY = "AndroidDownloadManager"
    private var downloaded = 0L
    private val fileName: String

    init {
        val file = File(PATH, BASE_DIRECTORY)
        if (!file.exists())
            file.mkdirs()

        url.toString().also { fileName = it.split("/")[it.split("/").lastIndex] }
    }


    private val downloadFile: File = File(PATH + "/" + BASE_DIRECTORY, fileName)
    private val fileOutputStream = FileOutputStream(downloadFile, downloadFile.exists())
    private val bufferedOutputStream = BufferedOutputStream(fileOutputStream, 1024)
    private var bufferedInputStream: BufferedInputStream? = null
    private val timer = Timer()


    fun start(onDownloading: OnDownloading) {
        onDownloading.onStart(fileName)
        try {
            val conection = url.openConnection()
            setConectionProperty(conection)

            conection.doInput = true
            conection.doOutput = true

            bufferedInputStream = BufferedInputStream(conection.getInputStream())

            startStreaming(onDownloading, conection.contentLength)

            bufferedOutputStream.flush()
            bufferedOutputStream.close()
            fileOutputStream.close()
            onDownloading.onSuccessful("download seccesfully finished at ${downloadFile.absolutePath} ")
        } catch (e: Exception) {
            onDownloading.onError(
                Error(
                    "somthing went wrong!!! \n please try again \n ${e.message}",
                    e
                )
            )
        }
    }

    private fun setConectionProperty(conection: URLConnection?): Boolean {
        conection?.let {
            if (downloadFile.exists()) {
                downloaded = downloadFile.length()
                conection.setRequestProperty(
                    "Range",
                    "bytes=" + (downloadFile.length()) + "-"
                )
            } else {
                conection.setRequestProperty("Range", "bytes=" + downloaded + "-");
            }
            conection.connectTimeout = TIME_OUT
            return true
        }
        return false

    }

    private fun startStreaming(onDownloading: OnDownloading, length: Int) {
        val buf = ByteArray(1024)
        var count = 0
        bufferedInputStream?.let {
            startTiming(onDownloading, length)
            while (it.read(buf).also { count = it } != -1) {
                downloaded += count
                bufferedOutputStream.write(buf, 0, count)
            }
        }
    }

    private fun startTiming(onDownloading: OnDownloading, length: Int) {
        timer.scheduleAtFixedRate(object : TimerTask() {
            var a = 0L
            override fun run() {
                try {
                    val byteInSecound: Float = downloaded.toFloat() - a
                    a = downloaded
                    onDownloading.onUpdate(
                        (downloaded * 100 / length).toInt(),
                        byteInSecound
                    )
                } catch (e: Exception) {
                }
            }
        }, 0L, 1000L)
    }
}

public interface OnDownloading {
    fun onStart(fileName: String)
    fun onError(error: Error)
    fun onSuccessful(response: String)
    fun onUpdate(percentOfDownload: Int, speed: Float)
}