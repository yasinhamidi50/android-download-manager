package com.example.androiddownloadmanager.network


import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.http.GET

private const val BASE_URL = "https://project.yasinhamidi.ir"

private const val API_KEY = "98d7dff1d3aec8baf0bfe575fd80a8a0"

enum class CryptoInterval(val interval: String) { H1("1h"), D1("1d"), D7("7d"), D30("30d") }
enum class CryptoConvert(val convert: String) { USD("USD"), EUR("EUR") }


private val retrofit = Retrofit.Builder()
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface CryptoApiService {
    @GET("MafiaApp/mafia.apk")
    fun getCryptoCurrencies(
    ): Deferred<ResponseBody>



}

object CryptoApi {
    val cryptoApiService: CryptoApiService by lazy { retrofit.create(CryptoApiService::class.java) }
}
